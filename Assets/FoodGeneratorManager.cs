﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodGeneratorManager : MonoBehaviour
{
    public List<FoodItem> foodsGenerated;
    public static FoodGeneratorManager instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        GameStatesControl.instance.actionGameStart += ClearList;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FoodAddToList(FoodItem f)
    {
        foodsGenerated.Add(f);
    }
    public void FoodRemoveFromList(FoodItem f)
    {
        foodsGenerated.Remove(f);
    }
    public void ClearList()
    {
        while(foodsGenerated.Count > 0)
        {
            FoodItem f = foodsGenerated[0];
            foodsGenerated.RemoveAt(0);
            Destroy(f.gameObject);
        }
    }
}
