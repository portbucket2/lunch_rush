﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelValuesCurrent : MonoBehaviour
{
    public static LevelValuesCurrent instance;
    public List<int> foodOrderIndexesInThisLevel;
    LevelValuesAssignManager levelValuesAssignManager;

    private void Awake()
    {
        instance = this;
        levelValuesAssignManager = GetComponent<LevelValuesAssignManager>();

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LevelValuesGetAtGameStart()
    {
        foodOrderIndexesInThisLevel.Clear();
        List<int> indices = new List<int>();
        for (int i = 0; i < levelValuesAssignManager.levelValues[GameManagementMain.instance.levelIndex].foodOrderIndexesInThisLevel.Count; i++)
        {
            indices.Add(levelValuesAssignManager.levelValues[GameManagementMain.instance.levelIndex].foodOrderIndexesInThisLevel[i]);
        }

        foodOrderIndexesInThisLevel = indices;
    }
}
