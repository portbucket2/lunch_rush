﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodOrderManager : MonoBehaviour
{
    public List<FoodOrder> foodOrders;
    public static FoodOrderManager instance;
    public GameObject[] tables;
    private void Awake()
    {
        instance = this;
    }
    [System.Serializable]
    public class FoodOrder
    {
        public int foodIndex;
        public int tableIndex;
        public CustomerMover customer;

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OrderTake(int foodIndex , CustomerMover customer)
    {
        FoodOrder f = new FoodOrder();
        f.foodIndex = foodIndex;
        f.customer = customer;
        f.tableIndex =  customer.pathwayCurrent.pathwayIndex;

        foodOrders.Add(f);
    }
    public void OrderDeliver(int i)
    {
        FoodOrder f = foodOrders[i];
        //f.customer.LeaveTheSeat();
        f.customer.GetComponent<CustomerStatesManager>().ChangeCustomerStateTo(CustomerStates.FoodReady);
        foodOrders.RemoveAt(i);

    }
    public bool EvaluateMergedFoodIsAnOrderOrNot(FoodItem f)
    {
        bool r = false;
        for (int i = 0; i < foodOrders.Count; i++)
        {
            if(foodOrders[i].foodIndex == f.foodCode)
            {
                r = true;
                
                FoodPlatesManager.instance.MakePlateReady(foodOrders[i].tableIndex , f , foodOrders[i].customer);
                OrderDeliver(i);
                break;
            }
        }
        return r;
    }
}
