﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CustomerMover : MonoBehaviour
{
    public bool moving;
    public PathPoint target;
    public PathwayForCustomers pathwayCurrent;
    public float moveSpeed;
    public Action<CustomerMover> actionCustomerDeath;
    public Action<CustomerMover> actionCustomerLeave;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MoveToTagget();

        //if (Input.GetKeyDown(KeyCode.V))
        //{
        //    LeaveTheSeat();
        //}
    }
    public void MoveToTagget()
    {
        if (!target)
        {
            return;
        }
        if (moving)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime * moveSpeed);
            transform.rotation = Quaternion.Lerp(transform.rotation, target.transform.rotation, Time.deltaTime * moveSpeed*3f);
            if(transform.position == target.transform.position)
            {
                
                transform.rotation = target.transform.rotation;
                ReachedTargetFun();
            }
        }
    }
    public void ReachedTargetFun()
    {
        MovePause();
        ReachedFunAtPathPoint(target);
    }
    public void MoveStart()
    {
        moving = true;
    }
    public void MovePause()
    {
        moving = false;
    }
    public void TargetAssign(PathPoint p)
    {
        target = p;
    }
    public void LeaveTheSeat()
    {
        MoveStart();
        if(target!= null)
        {
            target.pathwayParent.EmptyIt();
        }

        CustomerManager.instance.CallASpawn();
        actionCustomerLeave?.Invoke(this);
    }
    public void ReachedFunAtPathPoint(PathPoint p)
    {
        if (p.nextPathPoint)
        {
            target = p.nextPathPoint;

        }
        if (p.isSitPoint)
        {
            GetComponent<CustomerStatesManager>().ChangeCustomerStateTo(CustomerStates.ReachedSeat);
            
            return;
        }
        if (p.isEndPoint)
        {
            actionCustomerDeath?.Invoke(this);
            Destroy(this.gameObject);
            return;
        }
        MoveStart();
    }
}
