﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plate : MonoBehaviour
{
    public GameObject target;
    public bool moving;
    public float moveSpeed;
    public int plateIndex;
    public FoodItem myFood;
    public CustomerStatesManager myCustomer;
    float progression;
    Vector3 oldPos;
    public AnimationCurve ac;
    float jumpHeight;
    Vector3 foodOldPos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (moving)
        //{
        //    
        //    transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime * moveSpeed);
        //    transform.rotation = Quaternion.Lerp(transform.rotation, target.transform.rotation, Time.deltaTime * moveSpeed * 3f);
        //    if (transform.position == target.transform.position)
        //    {
        //
        //        transform.rotation = target.transform.rotation;
        //        
        //    }
        //}
        if (moving)
        {
            if(progression < 1)
            {
                progression += Time.deltaTime * moveSpeed;
            }
            else
            {
                progression = 1;
                moving = false;

                PlateTargetReachFun();
            }
            transform.position = Vector3.Lerp(oldPos, target.transform.position, progression) +( transform.up*ac.Evaluate(progression)* jumpHeight);

        }
    }

    public void PlateMoveTo(GameObject t , float h = 0)
    {
        jumpHeight = h;
        oldPos = transform.position;
        target = t;
        progression = 0;
        moving = true;
    }
    public void PlateRemoveFromTable()
    {
        PlateMoveTo(FoodPlatesManager.instance.outPos);
        myCustomer.actionExitting -= PlateRemoveFromTable;
        Destroy(myFood.gameObject);
        myFood = null;
    }
    public void PlaceFoodIntoPlate()
    {
        myFood.gameObject.transform.SetParent(transform);
        foodOldPos = myFood.transform.localPosition;
        //myFood.transform.localPosition = new Vector3(0, 0, 0);
        StartCoroutine(FoodToPlateCoroutine());
    }
    IEnumerator FoodToPlateCoroutine(float t = 0)
    {
        yield return null;
        t += Time.deltaTime * moveSpeed * 2;
        myFood.transform.localPosition = Vector3.Lerp(foodOldPos, Vector3.zero, t) + (transform.up * ac.Evaluate(t) * 1);
        if (t< 1)
        {
            StartCoroutine(FoodToPlateCoroutine(t));
        }
        else
        {
            t = 1;
            myFood.transform.localPosition = Vector3.Lerp(myFood.transform.localPosition, Vector3.zero, t);
            FoodPlatesManager.instance.EnableServeButton();
        }
    }
    public void PlateTargetReachFun()
    {
        if(target == FoodPlatesManager.instance.readyPos)
        {
            PlaceFoodIntoPlate();
        }
        if (target == FoodPlatesManager.instance.tables[plateIndex] )
        {
            myCustomer.ChangeCustomerStateTo(CustomerStates.FoodServed);
            myCustomer.actionExitting += PlateRemoveFromTable;
        }
        if (target == FoodPlatesManager.instance.outPos)
        {

        }
    }
}
