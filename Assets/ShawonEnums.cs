﻿public enum FoodTypes
{
    Bread,Jam,Sandwitch,IceCream,Cherry,CherryCream
}

public enum CustomerStates
{
    Idle, MovingToSeat, ReachedSeat, Seated, Ordered, Waiting, FoodReady, FoodServed, FoodEating, EatingDone, Expression, StandToLeave, Exitting
}
public enum GameStates
{
    MenuIdle,
    GameStart,
    LevelStart,
    GamePlay,
    LevelResult,
    LevelEnd
}

public enum LevelResultType
{
    Success, Fail
}

