﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodTypesManager : MonoBehaviour
{
    public static FoodTypesManager instance;
    public List<FoodMergeFormula> foodMergeFormulas;
    public List<FoodItem> foodTypesAvailable;
    public List<FoodItem> foodTypesPrimary;

    [System.Serializable]
    public class FoodMergeFormula
    {
        public FoodItem inputFoodOne;
        public FoodItem inputFoodTwo;
        public FoodItem OutputFood;
    }

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
