﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class GameStatesControl : MonoBehaviour
{
    [SerializeField]
    private GameStates gameStateCurrent;

    public Action actionMenuIdle    ;
    public Action actionGameStart   ;
    public Action actionLevelStart  ;
    public Action actionGamePlay    ;
    public Action actionLevelResult ;
    public Action actionLevelEnd    ;


    public static GameStatesControl instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        AssignFuntionsToActions();
        ChangeGameStateTo(GameStates.MenuIdle);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ChangeGameStateTo(GameStates state , float t)
    {
        StartCoroutine(ChangeGameStateCoRoutine(state,  t));
    }
    public GameStates GetCurrentGameState()
    {
        return gameStateCurrent;
    }
    public IEnumerator ChangeGameStateCoRoutine(GameStates state , float t)
    {
        yield return new WaitForSeconds(t);
        ChangeGameStateTo(state);
    }
    public void ChangeGameStateTo(GameStates state)
    {
        if(gameStateCurrent == state)
        {
            //return;
        }
        gameStateCurrent = state;

        switch (state)
        {
            case GameStates.MenuIdle   :
                {
                    
                    actionMenuIdle   ?.Invoke();
                    
                    break;
                }
            case GameStates.GameStart  :
                {
                    actionGameStart  ?.Invoke();

                    ChangeGameStateTo(GameStates.LevelStart);

                    break;
                }
            case GameStates.LevelStart :
                {
                    
                    ChangeGameStateTo(GameStates.GamePlay ,0.5f );
                    
                    actionLevelStart ?.Invoke();
                    
                    break;
                }
            case GameStates.GamePlay   :
                {
                    actionGamePlay   ?.Invoke();
                    break;
                }
            case GameStates.LevelResult:
                {
                    actionLevelResult?.Invoke();

                    ChangeGameStateTo(GameStates.LevelEnd, 2);
                    break;
                }
            case GameStates.LevelEnd:
                {
                    actionLevelEnd?.Invoke();
                    //ReferenceMaster.instance.superMoveUnlockManager.SuperMoveUnlockLeveleEndFun();
                    break;
                }
        }
    }

    public void AssignFuntionsToActions()
    {
        actionMenuIdle += UiManage.instance.MenuIdlePanelAppear;
        actionMenuIdle += UiManage.instance.UiValuesUpdate;
        //actionMenuIdle += InputMouseSystem.instance.TouchDisable;
        //actionMenuIdle += ReferenceMaster.instance.superMoveUnlockManager.DetectUnlockAtMenuIdle;
        //actionMenuIdle += LevelManager.instance.MenuIdleFun;


        //
        actionGameStart += CustomerManager.instance.GameStartFun;
        actionGameStart += UiManage.instance.GamePlayPanelAppear;
        actionGameStart += UiManage.instance.UiValuesUpdate;
        actionGameStart += LevelValuesCurrent.instance.LevelValuesGetAtGameStart;
        //actionGameStart += LevelManager.instance.GameStartFun;
        //actionGameStart += LevelValuesAssignManager.instance.LevelDataTransferAtGameStart;
        //
        actionLevelStart += LevelManager.instance.LevelStartFun;
        //actionLevelStart += InputMouseSystem.instance.TouchEnable;
        actionLevelStart +=  GameManagementMain.instance.AnalyticsCallLevelStarted ;
        //
        
        actionLevelResult += UiManage.instance.LevelResultPanelAppear;
        //actionLevelResult += LevelManager.instance.LevelResultFunction;
        //actionLevelResult += InputMouseSystem.instance.TouchDisable;
        //
        actionLevelEnd += UiManage.instance.LevelEndPanelAppear;
        //actionLevelEnd += InputMouseSystem.instance.TouchDisable;
    }
}
