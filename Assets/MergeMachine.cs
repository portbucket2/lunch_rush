﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ShawonGameTools;

public class MergeMachine : MonoBehaviour
{
    public GameObject slotOne;
    public GameObject slotTwo;
    public GameObject slotOutput;
    public List<FoodItem> foodsToMerge;
    public FoodItem outputFood;
    public bool positioningFirst;
    public bool positioningSecond;

    public GameObject f;
    public GameObject g;
    public GameObject h;

    public static MergeMachine instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            TakeItForMerging(f.GetComponent<FoodItem>());
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            TakeItForMerging(g.GetComponent<FoodItem>());
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            TakeItForMerging(h.GetComponent<FoodItem>());
        }


        if (positioningFirst)
        {
            foodsToMerge[0].transform.position = Vector3.MoveTowards(foodsToMerge[0].transform.position, slotOne.transform.position, Time.deltaTime * 5);
            foodsToMerge[0].transform.rotation = Quaternion.RotateTowards(foodsToMerge[0].transform.rotation, slotOne.transform.rotation, Time.deltaTime * 200);
            if (Vector3.Distance(foodsToMerge[0].transform.position, slotOne.transform.position) < 0.1f)
            {
                foodsToMerge[0].transform.position = slotOne.transform.position;
                foodsToMerge[0].transform.rotation = slotOne.transform.rotation;
                positioningFirst = false;
            }
        }
        if (positioningSecond)
        {
            foodsToMerge[1].transform.position = Vector3.MoveTowards(foodsToMerge[1].transform.position, slotTwo.transform.position, Time.deltaTime * 5);
            foodsToMerge[1].transform.rotation = Quaternion.RotateTowards(foodsToMerge[1].transform.rotation, slotTwo.transform.rotation, Time.deltaTime * 200);
            if (Vector3.Distance(foodsToMerge[1].transform.position, slotTwo.transform.position) < 0.1f)
            {
                foodsToMerge[1].transform.position = slotTwo.transform.position;
                foodsToMerge[1].transform.rotation = slotTwo.transform.rotation;
                positioningSecond = false;
                StartMerging();
                //PerformMerge();
            }
        }
    }
    public void TakeItForMerging(FoodItem f)
    {
        if(foodsToMerge.Count >= 2)
        {
            return;
        }
        if(foodsToMerge.Count == 0)
        {
            positioningFirst = true;
            foodsToMerge.Add(f);
            f.EnableOnMergeMachine();
        }
        else if (foodsToMerge.Count == 1)
        {
            

            if(CheckMergeValidity(foodsToMerge[0], f))
            {
                positioningSecond = true;
                foodsToMerge.Add(f);
                f.EnableOnMergeMachine();
            }
            else
            {
                f.PhysicsResponsive();
                f.GetComponent<Rigidbody>().AddForce(transform.up * 100f + transform.forward * 500f + transform.right * (Random.Range(-100f, 100f)));
            }
        }
        
    } 

    public bool CheckMergeValidity(FoodItem item1, FoodItem item2)
    {
        bool res = false;
        for (int i = 0; i < FoodTypesManager.instance.foodMergeFormulas.Count; i++)
        {
            if(FoodTypesManager.instance.foodMergeFormulas[i].inputFoodOne.foodCode == item1.foodCode)
            {
                if (FoodTypesManager.instance.foodMergeFormulas[i].inputFoodTwo.foodCode == item2.foodCode)
                {
                    res = true;
                    outputFood = FoodTypesManager.instance.foodMergeFormulas[i].OutputFood;
                }
            
            }
            else if (FoodTypesManager.instance.foodMergeFormulas[i].inputFoodOne.foodCode == item2.foodCode)
            {
                if (FoodTypesManager.instance.foodMergeFormulas[i].inputFoodTwo.foodCode == item1.foodCode)
                {
                    res = true;
                    outputFood = FoodTypesManager.instance.foodMergeFormulas[i].OutputFood;
                }
            }

            if (res)
            {
                break;
            }
        }

        return res;
    }

    public void PerformMerge(GameObject outputObj)
    {
        //GameObject outputObj = Instantiate(outputFood.gameObject, slotOutput.transform.position, transform.rotation);
        FoodGeneratorManager.instance.FoodRemoveFromList(foodsToMerge[0]);
        FoodGeneratorManager.instance.FoodRemoveFromList(foodsToMerge[1]);
        Destroy(foodsToMerge[0].gameObject);
        Destroy(foodsToMerge[1].gameObject);

        foodsToMerge.Clear();
        outputFood = null;

        if (FoodOrderManager.instance.EvaluateMergedFoodIsAnOrderOrNot(outputObj.GetComponent<FoodItem>()))
        {
            //FoodOrderManager.instance.OrderDeliver(go.GetComponent<FoodItem>())
            //go.GetComponent<Rigidbody>().AddForce(-transform.forward * 500f + transform.right * (Random.Range(-100f, 100f)));
            outputObj.GetComponent<FoodItem>().PhysicsNonResponsive();
            //InputMouseSystem.instance.TouchDisable();
            RaycastSelectionSystem.instance.RaycastDisble();
        }
        else
        {
            outputObj.GetComponent<FoodItem>().PhysicsResponsive();
            outputObj.GetComponent<Rigidbody>().AddForce(transform.forward * 500f + transform.right * (Random.Range(-100f, 100f)));
        }
    }
    public void StartMerging()
    {
        StartCoroutine(MergingPrimaryFood(foodsToMerge[0].transform.gameObject, foodsToMerge[0].transform.position, slotOutput.transform.position, 0,1,0));
        StartCoroutine(MergingPrimaryFood(foodsToMerge[1].transform.gameObject, foodsToMerge[1].transform.position, slotOutput.transform.position, 0,1,0));
        GameObject go = Instantiate(outputFood.gameObject, slotOutput.transform.position, transform.rotation);
        go.transform.localScale = Vector3.zero;
        go.GetComponent<FoodItem>().PhysicsNonResponsive();
        StartCoroutine(OutputFoodGenerate(go, go.transform.position, slotOutput.transform.position, 0, 0, 1));
    }
    IEnumerator MergingPrimaryFood(GameObject go, Vector3 oldPos, Vector3 newPos, float t = 0,float scaleFrom = 0, float scaleTo = 1)
    {
        yield return null;
        t += Time.deltaTime * 5;
        if(t> 1)
        {
            t = 1;
        }
        go.transform.position = Vector3.Lerp(oldPos, newPos, t) ;
        go.transform.localScale = Vector3.one * Mathf.Lerp(scaleFrom, scaleTo, t);

        if (t < 1)
        {
            StartCoroutine(MergingPrimaryFood(go, oldPos,newPos, t, scaleFrom, scaleTo));
        }
        
    }
    IEnumerator OutputFoodGenerate(GameObject go, Vector3 oldPos, Vector3 newPos, float t = 0, float scaleFrom = 0, float scaleTo = 1)
    {
        yield return null;
        t += Time.deltaTime * 5;
        if (t > 1)
        {
            t = 1;
        }
        go.transform.position = Vector3.Lerp(oldPos, newPos, t);
        go.transform.localScale = Vector3.one * Mathf.Lerp(scaleFrom, scaleTo, t);

        if (t < 1)
        {
            StartCoroutine(OutputFoodGenerate(go, oldPos, newPos, t, scaleFrom, scaleTo));
        }
        if(t == 1)
        {
            PerformMerge(go);
        }

    }
    public void RejectTheFoodFromList(FoodItem f)
    {
        if (!f.OnMergeMachineOrNot())
        {
            return;
        }
        if (foodsToMerge.Contains(f))
        {
            foodsToMerge.Remove(f);
            f.DisableOnMergeMachine();
        }
        
    }
}
