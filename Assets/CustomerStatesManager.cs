﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CustomerStatesManager : MonoBehaviour
{
    public CustomerStates customerState;
    public Action actionExitting;
        
        // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ChangeCustomerStateTo(CustomerStates c)
    {
        customerState = c;
        switch (customerState)
        {
            case CustomerStates.Idle:
                {
                    break;
                }
            case CustomerStates.MovingToSeat:
                {
                    break;
                }
            case CustomerStates.ReachedSeat:
                {
                    ChangeCustomerStateTo(CustomerStates.Seated, 0.5f);
                    break;
                }
            case CustomerStates.Seated:
                {
                    ChangeCustomerStateTo(CustomerStates.Ordered, 0.5f);
                    break;
                }
            case CustomerStates.Ordered:
                {
                    FoodOrderManager.instance.OrderTake(GetComponent<CustomerFoodOrder>().orderedFoodIndex, GetComponent<CustomerMover>());
                    ChangeCustomerStateTo(CustomerStates.Waiting, 0.5f);
                    break;
                }
            case CustomerStates.Waiting:
                {
                    break;
                }
            case CustomerStates.FoodReady:
                {
                    break;
                }
            case CustomerStates.FoodServed:
                {
                    ChangeCustomerStateTo(CustomerStates.FoodEating, 0.25f);
                    break;
                }
            case CustomerStates.FoodEating:
                {
                    ChangeCustomerStateTo(CustomerStates.EatingDone, 0.25f);
                    break;
                }
            case CustomerStates.EatingDone:
                {
                    ChangeCustomerStateTo(CustomerStates.Expression, 0.25f);
                    break;
                }
            case CustomerStates.Expression:
                {
                    ChangeCustomerStateTo(CustomerStates.StandToLeave, 0.25f);
                    break;
                }
            case CustomerStates.StandToLeave:
                {
                    ChangeCustomerStateTo(CustomerStates.Exitting, 0.25f);
                    break;
                }
            case CustomerStates.Exitting:
                {
                    GetComponent<CustomerMover>().LeaveTheSeat();
                    actionExitting?.Invoke();
                    break;
                }
        }
    }
    IEnumerator StateChangeCoRoutine(CustomerStates c , float t)
    {
        yield return new WaitForSeconds(t);
        ChangeCustomerStateTo(c);
    }
    public void ChangeCustomerStateTo(CustomerStates c, float t)
    {
        StartCoroutine(StateChangeCoRoutine(c, t));
    }
}
