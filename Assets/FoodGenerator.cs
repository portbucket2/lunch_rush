﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodGenerator : MonoBehaviour
{
    public int nextFoodIndex;
    public float interval;
    public GameObject foodsHolder;
    // Start is called before the first frame update
    void Start()
    {
        GameStatesControl.instance.actionGamePlay += FoodGeneratorStart;
        GameStatesControl.instance.actionLevelResult += FoodGeneratorStop;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator GenerateFoodCoroutine(float t)
    {
        nextFoodIndex = Random.Range(0, FoodTypesManager.instance.foodTypesPrimary.Count);
        yield return new WaitForSeconds(t);
        GameObject go = Instantiate(FoodTypesManager.instance.foodTypesPrimary[nextFoodIndex].gameObject, transform.position, transform.rotation);
        go.GetComponent<Rigidbody>().AddForce(transform.forward * (Random.Range(50f, 300f)) + transform.right*(Random.Range(-100f, 100f)));
        go.transform.SetParent(foodsHolder.transform);
        FoodGeneratorManager.instance.FoodAddToList(go.GetComponent<FoodItem>());
        StartCoroutine(GenerateFoodCoroutine(interval));
    }
    public void FoodGeneratorStart()
    {
        StartCoroutine(GenerateFoodCoroutine(interval));
    }
    public void FoodGeneratorStop()
    {
        StopAllCoroutines();
    }
}
