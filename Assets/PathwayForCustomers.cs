﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathwayForCustomers : MonoBehaviour
{
    public List<PathPoint> pathPoints;
    public int pathwayIndex;
    public bool occupied;

    private void Awake()
    {
        AssignNextPointsToPathPoints();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void AssignNextPointsToPathPoints()
    {
        for (int i = 0; i < pathPoints.Count; i++)
        {
            if(i > 0)
            {
                pathPoints[i - 1].nextPathPoint = pathPoints[i];
            }
            pathPoints[i].pathwayParent = this;
        }
    }
    public void OccupyIt()
    {
        occupied = true;
    }
    public void EmptyIt()
    {
        occupied = false;
    }
}
