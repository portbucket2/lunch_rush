﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelValuesAssignManager : MonoBehaviour
{
    public List<LevelValues> levelValues;

    [System.Serializable]
    public class LevelValues
    {
        public string title;
        public List<int> foodOrderIndexesInThisLevel;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
