﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;
    public LevelResultType levelResultCurrent;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LevelStartFun()
    {
        CustomerManager.instance.InitialCustomerCallAtLevelStart();
    }
    public void SetLevelResultTypeTo(LevelResultType t)
    {
        levelResultCurrent = t;
        switch (levelResultCurrent)
        {
            case LevelResultType.Success:
                {
                    GameStatesControl.instance. actionLevelResult += UiManage.instance.UiSuccessPanelsAppear;
                    break;
                }
            case LevelResultType.Fail:
                {
                    GameStatesControl.instance.actionLevelResult += UiManage.instance.UiFailedPanelsAppear;
                    break;
                }
        }
    }
    
}
