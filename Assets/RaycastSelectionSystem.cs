﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ShawonGameTools;

public class RaycastSelectionSystem : MonoBehaviour
{
    public LayerMask upHitLayer;
    public LayerMask foodLayer;
    public GameObject grabbedFoodObj;
    public bool raycastEnabled;

    public static RaycastSelectionSystem instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        InputMouseSystem.instance.actionTouchHolded += RaycastAtUpScreen;
        InputMouseSystem.instance.actionTouchIn += RaycastAtFoods;
        InputMouseSystem.instance.actionTouchOut += ReleaseGrabbedFoodOnMachine;
        GameStatesControl.instance.actionLevelStart += RaycastEnable;
        GameStatesControl.instance.actionLevelResult += RaycastDisble;
    }

    // Update is called once per frame
    void Update()
    {
        //RaycastAtUpScreen();
    }

    public void RaycastAtUpScreen()
    {
        if (!raycastEnabled)
        {
            return;
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, upHitLayer))
        {
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            //Debug.Log("Did Hit");
            if(grabbedFoodObj != null)
            {
                grabbedFoodObj.transform.position = hit.point;
            }
        }
        else
        {
           // Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            //Debug.Log("Did not Hit");
        }
    }

    public void RaycastAtFoods()
    {
        if (!raycastEnabled)
        {
            return;
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, foodLayer))
        {
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            //Debug.Log("Did Hit Food");
            grabbedFoodObj = hit.transform.gameObject;
            grabbedFoodObj.GetComponent<FoodItem>().PhysicsNonResponsive();
            MergeMachine.instance.RejectTheFoodFromList(grabbedFoodObj.GetComponent<FoodItem>());
        }
        else
        {
            // Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            //Debug.Log("Did not Hit");
            grabbedFoodObj = null;
        }
    }

    public void ReleaseGrabbedFoodOnMachine()
    {
        if(grabbedFoodObj == null)
        {
            return;
        }
        float d = Vector3.Distance(MergeMachine.instance.gameObject.transform.position, grabbedFoodObj.transform.position);
        if (d > 2)
        {
            grabbedFoodObj.GetComponent<FoodItem>().PhysicsResponsive();
            return;
        }
        MergeMachine.instance.TakeItForMerging(grabbedFoodObj.GetComponent<FoodItem>());
    }
    public void RaycastEnable()
    {
        raycastEnabled = true;
    }
    public void RaycastDisble()
    {
        raycastEnabled = false;
    }
}
