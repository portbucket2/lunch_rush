﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodItem : MonoBehaviour
{
    public int foodCode;
    public FoodTypes foodType;
    bool OnMergeMachine;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public bool OnMergeMachineOrNot()
    {
        return OnMergeMachine;
    }
    public void EnableOnMergeMachine()
    {
        OnMergeMachine = true;
    }
    public void DisableOnMergeMachine()
    {
        OnMergeMachine = false;
    }
    public void PhysicsResponsive()
    {
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<Rigidbody>().isKinematic = false;

    }
    public void PhysicsNonResponsive()
    {
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().isKinematic = true;

    }
}
