﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ShawonGameTools;

public class FoodPlatesManager : MonoBehaviour
{
    public Plate[] plates;
    public GameObject[] tables;
    public Plate readyPlate;
    public int readyTableIndex;

    public GameObject initialPos;
    public GameObject readyPos;
    public GameObject outPos;

    public static FoodPlatesManager instance;
    public GameObject buttonServe;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            ServePlate();
        }
    }
    public void MakePlateReady(int tableIndex , FoodItem f, CustomerMover c)
    {
        plates[tableIndex].transform.position = initialPos.transform.position;
        plates[tableIndex].PlateMoveTo(readyPos);
        readyPlate = plates[tableIndex];
        readyTableIndex = tableIndex;

        plates[tableIndex].myFood = f;
        plates[tableIndex].myCustomer = c.GetComponent<CustomerStatesManager>();
    }
    public void ServePlate()
    {
        if(readyPlate == null)
        {
            return;
        }
        readyPlate.PlateMoveTo(tables[readyTableIndex], 2);
        readyPlate = null;

        buttonServe.SetActive(false);
        RaycastSelectionSystem.instance.RaycastEnable();
        //InputMouseSystem.instance.TouchEnable();
    }
    public void EnableServeButton()
    {
        buttonServe.SetActive(true);
    }
    public void SendPlateToOutPos(int tableIndex)
    {
        plates[tableIndex].PlateMoveTo(outPos);
    }
}
