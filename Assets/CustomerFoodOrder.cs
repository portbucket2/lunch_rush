﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomerFoodOrder : MonoBehaviour
{
    public int orderedFoodIndex;
    public Text foodOrderText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void InitializeCanvas()
    {
        foodOrderText.text = FoodTypesManager.instance.foodTypesAvailable[orderedFoodIndex].foodType.ToString();
    }
}
