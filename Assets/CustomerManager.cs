﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerManager : MonoBehaviour
{
    public GameObject customerPrefab;
    public List<GameObject> customerMeshes;
    public int spawnPendingCount;
    public bool engagedSpawning;
    public float spawnInterval;
    public List<PathwayForCustomers> pathWays;
    public int customersSpawnCount;
    public List<GameObject> customersPresent;
    public static CustomerManager instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            CallASpawn();
        }
    }

    public void SpawnACustomer(int pathIndex = 0)
    {
        if(spawnPendingCount <= 0)
        {
            return;
        }
        spawnPendingCount -= 1;
        GameObject go =  Instantiate(customerPrefab, transform.position, transform.rotation);
        go.GetComponent<CustomerMover>().pathwayCurrent = pathWays[pathIndex];
        go.GetComponent<CustomerMover>().TargetAssign(pathWays[pathIndex].pathPoints[0]);
        pathWays[pathIndex].OccupyIt();
        go.GetComponent<CustomerMover>().MoveStart();
        go.GetComponent<CustomerStatesManager>().ChangeCustomerStateTo(CustomerStates.MovingToSeat);
        
        go.GetComponent<CustomerFoodOrder>().orderedFoodIndex = LevelValuesCurrent.instance.foodOrderIndexesInThisLevel[customersSpawnCount];
        go.GetComponent<CustomerFoodOrder>().InitializeCanvas();

        customersPresent.Add(go);
        go.GetComponent<CustomerMover>().actionCustomerLeave += RemoveCustomerFromAliveList;
        customersSpawnCount += 1;
        engagedSpawning = true;
        StartCoroutine(SpawnDelayTimeStart(spawnInterval));
    }
    IEnumerator SpawnDelayTimeStart(float t)
    {
        yield return new WaitForSeconds(t);
        engagedSpawning = false;
        if(spawnPendingCount > 0)
        {
            int p = FindEmptyPathway();
            if(p >= 0)
            {
                SpawnACustomer(p);
            }
            
        }
    }
    public void RemoveCustomerFromAliveList(CustomerMover c)
    {
        customersPresent.Remove(c.gameObject);
        if (customersPresent.Count <= 0)
        {
            LevelManager.instance.SetLevelResultTypeTo(LevelResultType.Success);
            GameStatesControl.instance.ChangeGameStateTo(GameStates.LevelResult);
        }
    }
    public void KillAllCustomers()
    {
        while(customersPresent.Count > 0)
        {
            GameObject go = customersPresent[0].gameObject;
            customersPresent.RemoveAt(0);
            Destroy(go.gameObject);
        }
    }
    public void CallASpawn()
    {
        if(customersSpawnCount >= LevelValuesCurrent.instance.foodOrderIndexesInThisLevel.Count)
        {
            return;
        }

        spawnPendingCount += 1;
        if (!engagedSpawning)
        {
            //SpawnACustomer();
            engagedSpawning = true;
            StartCoroutine(SpawnDelayTimeStart(0));
        }
    }
    public int FindEmptyPathway()
    {
        int r = -1;
        for (int i = 0; i < pathWays.Count; i++)
        {
            if (!pathWays[i].occupied)
            {
                r = i;
                break;
            }
        }

        return r;
    }
    public void InitialCustomerCallAtLevelStart()
    {
        int c = LevelValuesCurrent.instance.foodOrderIndexesInThisLevel.Count;
        for (int i = 0; i < 3; i++)
        {
            if (i < c)
            {
                CallASpawn();
            }
        }
        //CallASpawn();
        //CallASpawn();
        //CallASpawn();
    }
    public void GameStartFun()
    {
        KillAllCustomers();
        customersSpawnCount = 0;
    }
}
